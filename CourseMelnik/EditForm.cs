﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseMelnik
{
    public partial class EditForm : Form
    {
        public event Action<Task, EditForm> Save;
        public event Action<Task, EditForm> JustClose;
        public event Action<Task, EditForm> Cancel;
        public enum EditType
        {
            Edit, New
        }

        readonly EditType _editmode;
        readonly Task _task;
        public EditForm(EditType EditMode, Task StartValue)
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(EditForm_FormClosing);
            _editmode = EditMode;
            _task = StartValue;
            Fill();
            switch (_editmode)
            {
                case EditType.New:
                    {
                        _task.ID = -1;
                        break;
                    }
                case EditType.Edit:
                    {

                        break;
                    }
            }
        }

        public Form1 Form1
        {
            get => default;
            set
            {
            }
        }

        private void UpdateTask()
        {
            _task.Date = dtStartDateTime.Value;
            _task.Header = t_header.Text;
            _task.IsDone = t_IsDone.Checked;
            _task.Body = t_memo.Text;
            _task.Worker = t_worker.Text;


        }

        private void Fill()
        {
            dtStartDateTime.Value = _task.Date;
            t_header.Text = _task.Header;
            t_IsDone.Checked = _task.IsDone;
            t_memo.Text = _task.Body;
            t_worker.Text = _task.Worker;
        }
        private void ЗакритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateTask();
                JustClose.Invoke(_task, this);
            }
            catch (Exception)
            {
            }
        }
        private void СохранитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateTask();
                Save.Invoke(_task, this);
            }
            catch (Exception)
            {
            }
        }
        private void ВідмінитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateTask();
                Cancel.Invoke(_task, this);
            }
            catch (Exception)
            {
            }
        }
        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Memo_TextChanged(object sender, EventArgs e)
        {

        }
        void EditForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
        private void Panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void t_memo_TextChanged(object sender, EventArgs e)
        {

        }

        private void t_IsDone_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void dtStartDateTime_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
