﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseMelnik;
using System.Windows.Forms;

namespace CourseMelnik
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
