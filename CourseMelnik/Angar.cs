﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseMelnik
{
    public class Angar
    {
        System.Collections.ObjectModel.Collection<Task> Array;
        public Angar()
        {
            Array = new System.Collections.ObjectModel.Collection<Task>();
        }
        public System.Collections.ObjectModel.Collection<Task> Storage
        {
            get { return Array; }
            set { Array = value; }
        }

        internal DataEngine DataEngine
        {
            get => default;
            set
            {
            }
        }

        public static Angar operator +(Task obj, Angar obj2)
        {
            obj2.Storage.Add(obj);
            return obj2;
        }
    }
}
