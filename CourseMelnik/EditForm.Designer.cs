﻿namespace CourseMelnik
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelN = new System.Windows.Forms.Label();
            this.t_memo = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.t_header = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.t_worker = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dtStartDateTime = new System.Windows.Forms.DateTimePicker();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.t_IsDone = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(62)))), ((int)(((byte)(70)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.закрытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.отменитьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(458, 25);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.закрытьToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.закрытьToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(67, 21);
            this.закрытьToolStripMenuItem.Text = "Закрити";
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.ЗакритиToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.сохранитьToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(74, 21);
            this.сохранитьToolStripMenuItem.Text = "Зберегти";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.СохранитиToolStripMenuItem_Click);
            // 
            // отменитьToolStripMenuItem
            // 
            this.отменитьToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.отменитьToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.отменитьToolStripMenuItem.Name = "отменитьToolStripMenuItem";
            this.отменитьToolStripMenuItem.Size = new System.Drawing.Size(75, 21);
            this.отменитьToolStripMenuItem.Text = "Відмінити";
            this.отменитьToolStripMenuItem.Click += new System.EventHandler(this.ВідмінитиToolStripMenuItem_Click);
            // 
            // labelN
            // 
            this.labelN.AutoSize = true;
            this.labelN.BackColor = System.Drawing.Color.Transparent;
            this.labelN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelN.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelN.Location = new System.Drawing.Point(-89, 15);
            this.labelN.Name = "labelN";
            this.labelN.Size = new System.Drawing.Size(71, 15);
            this.labelN.TabIndex = 10;
            this.labelN.Text = "Виконано";
            // 
            // t_memo
            // 
            this.t_memo.Location = new System.Drawing.Point(0, 220);
            this.t_memo.Name = "t_memo";
            this.t_memo.Size = new System.Drawing.Size(458, 194);
            this.t_memo.TabIndex = 11;
            this.t_memo.Text = "";
            this.t_memo.TextChanged += new System.EventHandler(this.t_memo_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(7, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(221, 42);
            this.panel2.TabIndex = 12;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel2_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(64, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Заголовок";
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.t_header);
            this.panel3.Location = new System.Drawing.Point(234, 28);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(224, 42);
            this.panel3.TabIndex = 13;
            // 
            // t_header
            // 
            this.t_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(181)))));
            this.t_header.ForeColor = System.Drawing.Color.Black;
            this.t_header.Location = new System.Drawing.Point(24, 12);
            this.t_header.Name = "t_header";
            this.t_header.Size = new System.Drawing.Size(159, 20);
            this.t_header.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Controls.Add(this.label2);
            this.panel6.Location = new System.Drawing.Point(7, 76);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(221, 42);
            this.panel6.TabIndex = 14;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel6_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(64, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Виконавець";
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.t_worker);
            this.panel5.Location = new System.Drawing.Point(234, 76);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(224, 42);
            this.panel5.TabIndex = 13;
            // 
            // t_worker
            // 
            this.t_worker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(181)))));
            this.t_worker.ForeColor = System.Drawing.Color.Black;
            this.t_worker.Location = new System.Drawing.Point(24, 10);
            this.t_worker.Name = "t_worker";
            this.t_worker.Size = new System.Drawing.Size(159, 20);
            this.t_worker.TabIndex = 1;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Controls.Add(this.label3);
            this.panel9.Location = new System.Drawing.Point(7, 124);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(221, 42);
            this.panel9.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(81, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Дата";
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.Controls.Add(this.dtStartDateTime);
            this.panel8.Location = new System.Drawing.Point(237, 124);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(221, 42);
            this.panel8.TabIndex = 16;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel8_Paint);
            // 
            // dtStartDateTime
            // 
            this.dtStartDateTime.CustomFormat = "dd.MM.yyyy";
            this.dtStartDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartDateTime.Location = new System.Drawing.Point(21, 13);
            this.dtStartDateTime.Name = "dtStartDateTime";
            this.dtStartDateTime.ShowUpDown = true;
            this.dtStartDateTime.Size = new System.Drawing.Size(159, 20);
            this.dtStartDateTime.TabIndex = 5;
            this.dtStartDateTime.Value = new System.DateTime(2020, 3, 25, 0, 0, 0, 0);
            this.dtStartDateTime.ValueChanged += new System.EventHandler(this.dtStartDateTime_ValueChanged);
            // 
            // panel12
            // 
            this.panel12.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.label4);
            this.panel12.Location = new System.Drawing.Point(7, 172);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(221, 42);
            this.panel12.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(70, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Виконано";
            // 
            // panel11
            // 
            this.panel11.BackgroundImage = global::CourseMelnik.Properties.Resources.top_bg_v1;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.t_IsDone);
            this.panel11.Location = new System.Drawing.Point(237, 172);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(221, 42);
            this.panel11.TabIndex = 13;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel11_Paint);
            // 
            // t_IsDone
            // 
            this.t_IsDone.AutoSize = true;
            this.t_IsDone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(181)))));
            this.t_IsDone.Location = new System.Drawing.Point(94, 14);
            this.t_IsDone.Name = "t_IsDone";
            this.t_IsDone.Size = new System.Drawing.Size(15, 14);
            this.t_IsDone.TabIndex = 1;
            this.t_IsDone.UseVisualStyleBackColor = false;
            this.t_IsDone.CheckedChanged += new System.EventHandler(this.t_IsDone_CheckedChanged);
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(62)))), ((int)(((byte)(70)))));
            this.ClientSize = new System.Drawing.Size(458, 415);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.t_memo);
            this.Controls.Add(this.labelN);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditForm";
            this.Text = "Додавання запису";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отменитьToolStripMenuItem;
        private System.Windows.Forms.Label labelN;
        private System.Windows.Forms.RichTextBox t_memo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox t_header;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox t_worker;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DateTimePicker dtStartDateTime;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox t_IsDone;
    }
}