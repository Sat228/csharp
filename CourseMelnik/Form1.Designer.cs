﻿namespace CourseMelnik
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.iceButton1 = new IceButton.IceButton();
            this.iceButton6 = new IceButton.IceButton();
            this.iceButton2 = new IceButton.IceButton();
            this.iceButton4 = new IceButton.IceButton();
            this.iceButton5 = new IceButton.IceButton();
            this.iceButton3 = new IceButton.IceButton();
            this.tmenu = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.t_search = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tmenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup_2);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(62)))), ((int)(((byte)(70)))));
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = global::CourseMelnik.Properties.Resources.themify_e610_0__64;
            this.label1.Location = new System.Drawing.Point(13, 106);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 66);
            this.label1.TabIndex = 0;
            this.label1.Text = "Пошук";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // iceButton1
            // 
            this.iceButton1.BackgroundImage = global::CourseMelnik.Properties.Resources.themify_e68e_1__1024;
            this.iceButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iceButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.iceButton1.Location = new System.Drawing.Point(209, 0);
            this.iceButton1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.iceButton1.Name = "iceButton1";
            this.iceButton1.Size = new System.Drawing.Size(96, 95);
            this.iceButton1.TabIndex = 0;
            this.iceButton1.Tag = "Перегляд всіх записів";
            this.iceButton1.Load += new System.EventHandler(this.iceButton1_Load);
            // 
            // iceButton6
            // 
            this.iceButton6.BackgroundImage = global::CourseMelnik.Properties.Resources.themify_e668_0__1024;
            this.iceButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iceButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.iceButton6.Location = new System.Drawing.Point(104, 0);
            this.iceButton6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.iceButton6.Name = "iceButton6";
            this.iceButton6.Size = new System.Drawing.Size(96, 95);
            this.iceButton6.TabIndex = 5;
            this.iceButton6.Tag = "Показати справи на сьогодні";
            this.iceButton6.Load += new System.EventHandler(this.iceButton6_Load);
            // 
            // iceButton2
            // 
            this.iceButton2.BackgroundImage = global::CourseMelnik.Properties.Resources.themify_e61a_2__1024;
            this.iceButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iceButton2.Cursor = System.Windows.Forms.Cursors.Default;
            this.iceButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.iceButton2.Location = new System.Drawing.Point(0, 0);
            this.iceButton2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.iceButton2.Name = "iceButton2";
            this.iceButton2.Size = new System.Drawing.Size(96, 95);
            this.iceButton2.TabIndex = 1;
            this.iceButton2.Tag = "Додати запис";
            this.iceButton2.Load += new System.EventHandler(this.iceButton2_Load);
            // 
            // iceButton4
            // 
            this.iceButton4.BackgroundImage = global::CourseMelnik.Properties.Resources.themify_e646_0__1024;
            this.iceButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iceButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.iceButton4.Location = new System.Drawing.Point(521, 0);
            this.iceButton4.Name = "iceButton4";
            this.iceButton4.Size = new System.Drawing.Size(93, 98);
            this.iceButton4.TabIndex = 7;
            this.iceButton4.Tag = "Закрити";
            // 
            // iceButton5
            // 
            this.iceButton5.BackgroundImage = global::CourseMelnik.Properties.Resources.themify_e605_0__1024;
            this.iceButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iceButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.iceButton5.Location = new System.Drawing.Point(314, 3);
            this.iceButton5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.iceButton5.Name = "iceButton5";
            this.iceButton5.Size = new System.Drawing.Size(96, 95);
            this.iceButton5.TabIndex = 4;
            this.iceButton5.Tag = "Видалити запис";
            this.iceButton5.Load += new System.EventHandler(this.iceButton5_Load);
            // 
            // iceButton3
            // 
            this.iceButton3.BackgroundImage = global::CourseMelnik.Properties.Resources.themify_e605_0__1024;
            this.iceButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iceButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.iceButton3.Location = new System.Drawing.Point(418, 0);
            this.iceButton3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.iceButton3.Name = "iceButton3";
            this.iceButton3.Size = new System.Drawing.Size(96, 95);
            this.iceButton3.TabIndex = 6;
            this.iceButton3.Tag = "Видалити всі записи";
            // 
            // tmenu
            // 
            this.tmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(62)))), ((int)(((byte)(70)))));
            this.tmenu.Controls.Add(this.iceButton3);
            this.tmenu.Controls.Add(this.iceButton5);
            this.tmenu.Controls.Add(this.iceButton4);
            this.tmenu.Controls.Add(this.iceButton2);
            this.tmenu.Controls.Add(this.iceButton6);
            this.tmenu.Controls.Add(this.iceButton1);
            this.tmenu.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.tmenu.Location = new System.Drawing.Point(16, 1);
            this.tmenu.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tmenu.Name = "tmenu";
            this.tmenu.Size = new System.Drawing.Size(622, 101);
            this.tmenu.TabIndex = 4;
            this.tmenu.Paint += new System.Windows.Forms.PaintEventHandler(this.tmenu_Paint);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridView1.GridColor = System.Drawing.Color.Purple;
            this.dataGridView1.Location = new System.Drawing.Point(3, 175);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(635, 397);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Номер";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Дата";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 75;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Заголовок";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 130;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Опис";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 130;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Виконавець";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Виконано?";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(181)))));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(84, 17);
            this.toolStripStatusLabel1.Text = "Тех.підтримка";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(559, 575);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(101, 22);
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // t_search
            // 
            this.t_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(181)))));
            this.t_search.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.t_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.t_search.ForeColor = System.Drawing.Color.Black;
            this.t_search.Location = new System.Drawing.Point(90, 137);
            this.t_search.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.t_search.Multiline = true;
            this.t_search.Name = "t_search";
            this.t_search.Size = new System.Drawing.Size(207, 23);
            this.t_search.TabIndex = 0;
            this.t_search.TextChanged += new System.EventHandler(this.t_search_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(181)))));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Номер",
            "Заголовок",
            "Опис",
            "Дата",
            "Виконавець"});
            this.comboBox1.Location = new System.Drawing.Point(90, 108);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(207, 23);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(62)))), ((int)(((byte)(70)))));
            this.ClientSize = new System.Drawing.Size(647, 601);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.t_search);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tmenu);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximumSize = new System.Drawing.Size(1000, 640);
            this.MinimumSize = new System.Drawing.Size(663, 640);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Розпорядник часу";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tmenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
        private IceButton.IceButton iceButton1;
        private IceButton.IceButton iceButton6;
        private IceButton.IceButton iceButton2;
        private IceButton.IceButton iceButton4;
        private IceButton.IceButton iceButton5;
        private IceButton.IceButton iceButton3;
        private System.Windows.Forms.Panel tmenu;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TextBox t_search;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}

