﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseMelnik
{
    public class Task : Angar
    {
        long _id;
        string _worker, _body, _header;
        DateTime _Date;
        bool _isdone;
        public long ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Worker
        {
            get { return _worker; }
            set { _worker = value; }
        }
        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
        public bool IsDone
        {
            get { return _isdone; }
            set { _isdone = value; }
        }

        public object[] ToValues()
        {
            object[] a;
            a = new object[6];
            a[0] = ID;
            a[1] = Date;
            a[2] = Header;
            a[3] = Body;
            a[4] = Worker;
            a[5] = IsDone;
            return a;
        }
    }
}
